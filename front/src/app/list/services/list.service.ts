import { Injectable } from '@angular/core';
import {Users} from '../models/users';
import { Observable } from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';


@Injectable()
export class ListService {
  private url: string = "http://localhost:3000/api/user";

  constructor( private http: HttpClient) { }

  public getUsers(): Observable<any> {
    var data;
    return this.http.post<Users>(this.url,data);
  }

}