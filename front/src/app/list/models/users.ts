export class Users{
    public id:number;
    public firstName:String;
    public lastName:String;
    public email:String;
    public pass:String;
    public rol:number;
    public created_at:String;
    public deleted_at:String;
}