import { Component, OnInit } from '@angular/core';
import {ListService} from './services/list.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [ListService]
})
export class ListComponent implements OnInit {
  public users:any;

  constructor(private listUsers: ListService) { }

  ngOnInit() {
    this.listUsers.getUsers().subscribe(data=>{
      this.users = data;
      console.log(this.users)
    });
  }

}
