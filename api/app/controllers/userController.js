const http = require('https');

var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";
var dbo;
var db$ = "prueba";
var collection = "users";

exports.getUser = function (req, res) {
    var data = req.body;

    MongoClient.connect(url,
        { useUnifiedTopology: true }, function (err, db) {
            if (err) throw err;
            dbo = db.db(db$); dbo
                .collection(collection)
                .find({})
                .toArray(function (err, result) {
                    if (err) throw err;

                    db.close();
                    if(Object.keys(result).length == 0)
                        result = false;

                    res.set({ "Access-Control-Allow-Origin": "*" });
                    res.type("json");
                    res.send(result);
                });
        });
};