export class Console {

    Log(informacion: any, type: number) {
        //type 1 = error, type 2 = informacion, type 3 = informacion de juego, type = 4 Mensaje de entrada
        if (type == 1) {
            console.log('\x1b[31m%s\x1b[0m', "\n-----------------\nERROR " + informacion + "\n-----------------\n");
        } else if (type == 2) {
            console.log("-----------------\n" + this.Colors("cyan") + "INFORMACION: " + this.Colors("reset") + informacion + "\n-----------------");
        } else if (type == 3) {
            console.log('\x1b[33m%s\x1b[0m', "INFORMACION DE LOBBY: ", informacion);
        } else if (type == 4) {
            console.log('\x1b[32m%s\x1b[0m', "MENSAJE DE ENTRADA: ", informacion);
        } else if (type == 5) {
            console.log('\x1b[34m%s\x1b[0m', "MENSAJE DE SALIDA: ", informacion);
        }
    }

    Colors(color: string) {
        switch (color) {
            case "red": {
                return "\x1b[31m";
            }
            case "reset": {
                return "\x1b[0m";
            }
            case "underscore": {
                return "\x1b[4m";
            }
            case "blue": {
                return "\x1b[34m";
            }
            case "green": {
                return "\x1b[32m";
            }
            case "yellow": {
                return "\x1b[33m";
            }
            case "white": {
                return "\x1b[37m";
            }
            case "bggreen": {
                return "\x1b[42m";
            }
            case "bgred": {
                return "\x1b[41m";
            }
            case "bgyellow": {
                return "\x1b[43m";
            }
            case "blink": {
                return "\x1b[5m";
            }
            case "hidden": {
                return "\x1b[8m";
            }
            case "cyan": {
                return "\x1b[36m";
            }
            case "magenta": {
                return "\x1b[35m";
            }
        }
    }
}
