export class Colors {
    magenta: string = "\x1b[35m";
    cyan: string = "\x1b[36m";
    yellow: string = "\x1b[33m";
    green: string = "\x1b[32m";
    red: string = "\x1b[31m";
    white: string = "\x1b[37m";

    blink: string = "\x1b[5m";
    raya_al_piso: string = "\x1b[4m";

    bg_green: string = "\x1b[42m";
    bg_red: string = "\x1b[41m";
    bg_yellow: string = "\x1b[43m";


    reset: string = "\x1b[0m";
}
