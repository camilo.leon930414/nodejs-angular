/* version 1.0 */
const express = require("express");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var path = require('path');

const app = express();
var route = require("./app/routes/index");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
mongoose.Promise = global.Promise;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

app.use("/api", route);
/* puerto */
app.set('port', process.env.PORT || 3000);
/* listen del servidor */
app.listen(app.get('port'), () => {
  console.log(`El servidor está inicializado en el puerto ${app.get('port')}`);
});

